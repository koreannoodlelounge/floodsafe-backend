﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Http.Cors;
using Twilio;

namespace FloodSafe.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SmsController : ApiController
    {
        // GET: api/Sms

        private const string AccountSid = "AC29a4980d9c48ba19c3641979e50df675";
        private const string AuthToken = "7c5e60f974ecbbf1fc6fb1550da77750";
        private const string FromPhoneNum = "+9149304872";

        public class FloodSafeUser
        {
            public string PhoneNumber { get; set; }
        }

        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Sms/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Sms
        public void Post(FloodSafeUser user)
        {
            if (user != null)
            {
                var Twilio = new TwilioRestClient(
                    AccountSid,
                    AuthToken);

                var body = new StringBuilder()
                    .Append(user.PhoneNumber)
                    .Append(" has ran the flood safe simulation, give him a nudge!").ToString();

                var myNum = "+6421719660";
                //var myNum = user.PhoneNumber;

                var result = Twilio.SendMessage(
                    FromPhoneNum,
                    myNum,
                    body
                    );

                //Status is one of Queued, Sending, Sent, Failed or null if the number is not valid
                Trace.TraceInformation(result.Status);
            }
        }

        // PUT: api/Sms/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Sms/5
        public void Delete(int id)
        {
        }
    }
}
